public class Main {
    public static void main(String[] args) {
        Rectangle myVar;
        myVar = new Rectangle(3,4,8,3);

        System.out.println("Area of rectangle: " + myVar.area() +
                "\nPerimeter of rectangle: " + myVar.perimeter() );

        System.out.println("\nCoordinates of each corner in rectangle");
        myVar.corners();

        Circle myVar2;
        myVar2 = new Circle(10,2,1);
        System.out.println("\nArea of circle: " + myVar2.area() +
                "\nPerimeter of circle: " + myVar2.perimeter() );

        Circle myVar3;
        myVar3 = new Circle(2,4,5);

        if (Circle.intersect(myVar2,myVar3))
            System.out.println("These two circles intersects.");
        else
            System.out.println("These two circles does NOT intersect.");
    }
}
